##----------------------------------##
## Libraries, functions and authors ##
##----------------------------------##

## Author: Ieva Rauluseviciute & Kata Ferenc
## Affiliation: NCMM, UiO, Oslo, Norway

## Import functions
import os
import itertools
from snakemake.utils import R
import time
import numpy as np
import glob

##----------------------##
## Config file          ##
## snakemake --cores 30 ##
##----------------------##

configfile: "config.yaml"

##-------------------##
## Record start time ##
##-------------------##

start = time.time()

##-------------##
## Directories ##
##-------------##

MAIN_DIR_ABS = os.getcwd()
RESULTS_DIR = os.path.join(config["title"])

##-----------##
## Functions ##
##-----------##

## When pipeline is executed succesfully:
onsuccess:
    success_runtime = round((time.time() - start)/60,3)
    f = open("{dir}/run_time.txt".format(dir = RESULTS_DIR), "w+")
    print("; Running time in minutes:\n %s" % success_runtime, file = f)
    print("; Running time in minutes:\n %s" % success_runtime)
    print("\n##---------------------------##\n; Workflow finished, no errors!\n##---------------------------##\n")

## When error occurs:
onerror:
    print("; Running time in minutes:\n %s\n" % round((time.time() - start)/60,1))
    print("\n##-----------------##\n; An error occurred!\n##-----------------##\n")

## When starting the run:
onstart:
    print("\n##-----------------------------------------##\n; Reading input and firing up the analysis...\n##-----------------------------------------##\n")

##---------------##
## Analysis type ##
##---------------##

ANALYSIS_TYPE = config["ANALYSIS_TYPE"]

if ANALYSIS_TYPE == "bulk":
    print("; Running analysis on BULK data.")
elif ANALYSIS_TYPE == "single-cell":
    print("; Running analysis on SINGLE-CELL data.")
elif ANALYSIS_TYPE == "both":
    print("; Running analysis on both BULK and SINGLE-CELL data.")
else:
    print("\nERROR!\n; Invalid analysis type. Available: bulk, single-cell or both.\n")
    raise IOError("\n; Set analysis type in config.yaml and try again.\n")


##------------------##
## Data directories ##
##------------------##

BULK_DATA_DIR = config["BULK_READ_FOLDER"]
SC_DATA_DIR   = config["SINGLE_CELL_READ_FOLDER"]

##-----------##
## Variables ##
##-----------##

BULK_SAMPLE, = glob_wildcards(os.path.join(BULK_DATA_DIR, "fasta", "{bulk_sample}_R1.fastq.gz"))

# SC_SAMPLE,   = glob_wildcards(os.path.join(SC_DATA_DIR,"sample_config", "{sc_sample}.csv"))

if ANALYSIS_TYPE == "bulk":
    PEAK_TYPE = ["bulk"]
elif ANALYSIS_TYPE == "single-cell":
    PEAK_TYPE = ["sc"]
elif ANALYSIS_TYPE == "both":
    PEAK_TYPE = ["bulk", "sc"]
else:
    print("; Incorrect analysis type in the config.yaml")

##--------------##
## Output files ##
##--------------##

BULK_PEAK_FILES = os.path.join(RESULTS_DIR, "peak_calling_summary", "bulk_peak_files.txt")
# this peak annotation file for both bulk and sc? 
# should something differentiate between bulk and sc in rule all?
PEAK_ANNOTATION = os.path.join(RESULTS_DIR, "peak_annotation", "peak_annotation_chipseeker.pdf")

# maybe this is enough for forcing sc
# SC_MULTIOMICS_DIMRED = expand(RESULTS_DIR + "sc/Signac/{sc_sample}/dimensionality_reduction.png", sc_sample = SC_SAMPLE)


##----------##
## Rule all ##
##----------##

rule all:
    input:
        # BULK_PEAK_FILES
        PEAK_ANNOTATION

##############
## Includes ##
##############

include: "thymus_bulk_processing.sk"
# include: "thymus_sc_processing.sk"
include: "thymus_analysis.sk"


######################
## End of Snakefile ##
######################