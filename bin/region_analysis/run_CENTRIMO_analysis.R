#####################
## Load R packages ##
#####################

required.libraries <- c("bedr",
                        "data.table",
                        "dplyr",
                        "optparse",
                        "ggplot2",
                        "stringr",
                        "tidyr")

for (lib in required.libraries) {
  suppressPackageStartupMessages(library(lib, character.only=TRUE, quietly = T))
}

####################
## Read arguments ##
####################

option_list = list(
  
  make_option(c("-p", "--peak_file"), type = "character", default = NULL,
              help = "Path to the file with info on peak files. (Mandatory) ", metavar = "character"),
  
  make_option(c("-o", "--output_folder"), type = "character", default = NULL,
              help = "Output folder for CENTRIMO results. (Mandatory) ", metavar = "character"),
  
  make_option(c("-g", "--genome_fasta"), type = "character", default = NULL,
              help = "Genome fasta file. (Mandatory) ", metavar = "character"),
  
  make_option(c("-m", "--meme_motifs"), type = "character", default = NULL,
              help = "A path to meme motifs folder. (Mandatory) ", metavar = "character"),
  
  make_option(c("-c", "--chrom_sizes"), type = "character", default = NULL,
              help = "A chromosome sizes file. (Mandatory)", metavar = "character")
  
);

message("; Reading arguments from command line.")
opt_parser = OptionParser(option_list = option_list)
opt = parse_args(opt_parser)

########################
## Set variable names ##
########################

peak.file      <- opt$peak_file
output.folder  <- opt$output_folder
genome.fasta   <- opt$genome_fasta
meme.folder    <- opt$meme_motifs
chrom.sizes    <- opt$chrom_sizes

## Syntax
# Rscript R-scripts/run_CENTRIMO_analysis.R \
# -p cutntag_RESULTS_2021_02_20/peak_calling_summary/peak_files_info.tsv \
# -o cutntag_TESTing_pipeline_1109/CENTRIMO_analysis \
# -g /storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa \
# -m /storage/mathelierarea/processed/ieva/projects/THYMUS/cutntag_pipeline/data/meme_motifs \
# -c /storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/hg38_chrom_sizes

## Debug
# setwd("/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/processed/ieva/projects/THYMUS/pipeline_cutntag")
# peak.file       <- "cutntag_RESULTS_2021_02_20/peak_calling_summary/peak_files_info.tsv"
# output.folder   <- "cutntag_RESULTS_2021_02_20/CENTRIMO_analysis"
# genome.fasta    <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/genome.fa"
# meme.folder     <- "data/meme_motifs"
# chrom.sizes     <- "/run/user/316574/gvfs/sftp:host=biotin2.hpc.uio.no/storage/mathelierarea/raw/UCSC/hg38/Sequence/WholeGenomeFasta/hg38_chrom_sizes"

#############################
## Create output directory ##
#############################
message("; Creating output directory.")
dir.create(output.folder, showWarnings = F, recursive = T)

fasta.output.folder <- file.path(output.folder, "fasta_files")
dir.create(fasta.output.folder, showWarnings = F, recursive = T)

bed.output.folder <- file.path(output.folder, "bed_files")
dir.create(bed.output.folder, showWarnings = F, recursive = T)

centrimo.output.folder <- file.path(output.folder, "centrimo_results")
dir.create(centrimo.output.folder, showWarnings = F, recursive = T)

##########################
## Getting sample names ##
##########################
message("; Getting sample names.")

sample_peak_info <- fread(peak.file,
                          header = TRUE)

#############################
## Looking for MEME motifs ##
#############################

meme_motifs <- list.files(meme.folder, recursive = TRUE)
message("; Centrimo will be run on ")
print(meme_motifs)

meme_motifs <- as.data.frame(meme_motifs) %>%
    mutate(TF = gsub(meme_motifs, pattern = ".meme", replacement = ""))

# print(sample_peak_info)

sample_peak_info <- left_join(sample_peak_info, meme_motifs,
                by = "TF") %>%
      drop_na(meme_motifs) %>%
      mutate(meme_motif = file.path(meme.folder, meme_motifs)) %>%
      dplyr::select(-meme_motifs) %>%
      mutate(bed_file = file.path(bed.output.folder, paste0(full_sample_name, ".bed"))) %>%
      mutate(fasta_file = file.path(fasta.output.folder, paste0(full_sample_name, ".fa")))
      
# print(sample_peak_info)
#######################
## Centrimo analysis ##
#######################

## Reading chrom_sizes:
chrom_sizes <- fread(file.path(chrom.sizes))
colnames(chrom_sizes) <- c("chr", "chrom_size")

for (i in 1:nrow(sample_peak_info)) {
  
  entry <- sample_peak_info[i,]
  
  bed_file <- fread(entry$peak_file_path)
  
  ## Extending and exporting extended regions:
  bed_file <- bed_file %>%
              mutate(summit_coord = V2 - V10) %>%
              mutate(LEFT_coord = summit_coord - 500) %>%
              mutate(RIGHT_coord = summit_coord + 500) %>%
              mutate(chr = V1) %>%
              dplyr::select(chr,
                             LEFT_coord,
                             RIGHT_coord)
              
  bed_file <- merge(bed_file, chrom_sizes,
                               by.x = "chr", by.y = "chr")
  
  bed_file <- bed_file %>%
    dplyr::filter(LEFT_coord >= 1 & RIGHT_coord <= chrom_size) %>%
    mutate(start = LEFT_coord,
           end = RIGHT_coord) %>%
    dplyr::select(chr,
                   start,
                   end)
  
  bed_file <- bedr.sort.region(as.data.frame(bed_file),
                         check.zero.based = FALSE,
                         check.chr = FALSE,
                         check.valid = FALSE,
                         check.merge = FALSE)
  
  fwrite(bed_file,
         file = entry$bed_file,
         col.names = FALSE,
         sep = "\t")
  
  ## Converting to fasta:
  getfasta_command <- paste("bedtools getfasta -fi",
                            genome.fasta,
                            "-bed",
                            entry$bed_file,
                            "-fo",
                            entry$fasta_file)
  
  message("; Converting to fasta:\n", getfasta_command)
  system(getfasta_command)
  
  ## Running centrimo:
  centrimo_output <- file.path(output.folder,
                               "centrimo_results",
                               entry$full_sample_name)
  
  centrimo_command <- paste("centrimo",
                            entry$fasta_file,
                            entry$meme_motif,
                            "--oc",
                            centrimo_output)
  message("; Running centrimo:\n", centrimo_command)
  system(centrimo_command)
  
}

############################################
## Writing sample peak info into the file ##
############################################
fwrite(sample_peak_info,
       file = file.path(output.folder, "centrimo_metadata.log"),
       col.names = TRUE,
       sep = "\t")

message("; All files processed.")

#######################
## End of the script ##
#######################