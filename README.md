# CUT&TAG DATA ANALYSIS PIPELINE #

Last updated: 2023 August 29

This README described the pipeline for bulk and single-cell CUT&Tag data analysis. The pipeline is able to run these analysis separately or together. Note that single cell analysis also includes accompanying single-cell gene expression analysis. Bellow you can find the analysis type specific input requirements and examples of the output.

The pipeline takes as an imput raw reads in fastq format and outputs peak regions together with various functional analysis results, such as gene region enrichment analysis with GREAT ([Cory Y McLean et al.](https://doi.org/10.1038/nbt.1630)) or regions annotations made with ChIPseeker ([G Yu et al.](http://dx.doi.org/10.1093/bioinformatics/btv145)).

The single-cell part currently runs from the sc_feature branch.

## Installation ##

* Python (version >= 3.8);
* R (version >= 3.6.1);
* Snakemake (version >= 5.26.1) (for installation follow the instructions [*here*](https://snakemake.readthedocs.io/en/stable/getting_started/installation.html));
* Bedtools (version >= 2.29.2) (for installation follow the instructions [*here*](https://bedtools.readthedocs.io/en/latest/content/installation.html)).

### Clone the repository ###

Clone the git repo from BitBucket:


```bash
git clone https://ievara@bitbucket.org/CBGR/pipeline_cutntag.git

cd pipeline_cutntag
```

### Install dependencies ###

You might already have some of these dependencies installed, but if you want to make sure, you can see the requirements and installation bellow.

* Install the required **R packages**:

```bash
Rscript bin/installation/install_R_packages_cutntag.R
```

* Install Intervene:

You can quickly install Intervene using pip:

```bash
pip install intervene
```

Read more about Intervene and other installation methods [here](https://intervene.readthedocs.io/en/latest/install.html).

* Install MACS2:

You can quickly install MAC2 using pip:

```bash
pip install MACS2
```

Read more about MAC2 [here](https://pypi.org/project/MACS2/).

* Cellranger

The pipeline requires Cellranger ARC to be installed. Read more about it [here](https://support.10xgenomics.com/single-cell-multiome-atac-gex/software/pipelines/latest/what-is-cell-ranger-arc).

* Signac

Signac should be installed separately following their installation guide [here](https://stuartlab.org/signac/articles/install).


## Setting-up the pipeline and required input ##

**Important: When working with sensitive single cell data you should run the analysis from a safe folder.** The single cell pipeline will output sensitive data in the folder you run the pipeline. Add this folder in the config under the name *SINGLE_CELL_READ_FOLDER*.

The pipeline is set up using configuration file ```config.yaml```. Here are the required fields:

* **title:** - the name of the folder the results will be outputed.
* **ANALYSIS_TYPE:** - analysis type, which indicates what data you would like to analyse. Must be one of following: *bulk*, *single-cell* or *both*.
* **BULK_READ_FOLDER:** and/or **SINGLE_CELL_READ_FOLDER:** - a path to the directory with raw sequencing reads in ```.fasta.gz``` format. The directory structure and file names should look like this:

```bash
## 1. Bulk data reads (forward and reverse) should be all in a folder fasta:
raw_reads_directory/
├── fasta
│   ├── sample1_R1.fastq.gz
│   ├── sample1_R2.fastq.gz
│   ├── sample2_R1.fastq.gz
│   ├── sample2_R2.fastq.gz
│   ├── IgG_R1.fastq.gz
│   ├── IgG_R2.fastq.gz
│   ├── ...

## 2. Single-cell data:

See below

```

* **BULK_METADATA:** - bulk data analysis requires a metadata file for the pipeline to know what backround was used for each sample. The table should look like this:

```bash
sample	    background	    tf_name	    condition
sample1	    IgG 	        AIRE	    1
sample2	    IgG 	        AIRE	    2
```

* **SINGLE_CELL_METADATA:** - single cell data analysis requires a metadata file for cellranger.

You should create one file for each 10x Mulitomics sample following the instructions of Cellranger [here](https://support.10xgenomics.com/single-cell-multiome-atac-gex/software/pipelines/latest/using/fastq-input). Place these files named as ```sample.csv``` in the ```data/sample_config/``` folder. One file per sample. 

Example from file called ```sample1.csv```:
~~~~
fastqs  sample  library_type    
path/to/GEX/fastq    sample_id_in_GEX_folder    Gene    Expression
path/to/CT/fastq    sample_id_in_CT_folder    Chromatin   Accessibility
~~~~


* **GENOME_INDEX:** - pre-indexed genome file (preferably hg38). This is used for read alignment.
* **BOWTIE2_CORES:** - a number of cores to use for alignment. *Note!* Be aware of how many Snakemake cores you are launching, because the total number of cores will be a multiplication of these two numbers.
* **PICARD:** - a path to ```picard.jar``` file.
* **CHROM_SIZES:** - a path to the file with chromosome sizes.
* **GENOME_FA:** - a path to the genome file (preferably hg38 to match the alignment).
* **MEME_FOLDER:** - a path to the directory with motifs in ```.meme``` format. These motifs are used by the pipeline to compute centrality. They must be some motifs of interest to be found in the called peaks (motifs are matched to the samples by TF name.
* **GENOME_VERSION:** - a genome version.
* **TARGET_GENES:** - a file with a list of target genes. These genes are searched for in the gene-region association study. This should be a simple text file with gene names as a column.


## Data preprocessing ##

In this section we shortly describe how the bulk and single-cell CUT&Tag data is processed and then handed-in to the downstream analysis step.

### BULK CUT&Tag ###

Bulk CUT&Tag data is processed largely based on [this data analysis tutorial](https://www.protocols.io/view/cut-amp-tag-data-processing-and-analysis-tutorial-bjk2kkye?step=12.2) by Ye Zheng, Kami Ahmad and Steven Henikoff. However, we also include the peak calling with **MACS2** and perform **SEACR** peak calling with both thresholding methods (*stringent* and *relaxed*).

### SINGLE-CELL CUT&Tag and gene expression ###

The single cell multiome data is processed using Cellranger ARC as if the CUT&Tag part would be scATAC.
Following mapping and initial quality control, the data is imported to Signac, where the multiomic analysis is followed found, still pretending that the scCUT&Tag part is scATAC [here](https://stuartlab.org/signac/articles/pbmc_multiomic).
Some modifications are implemented though, such as no nulceosome signal is shown as the data is in fact CUT&Tag.
Furthermore, cell type specific peaks are called following [this vignette](https://stuartlab.org/signac/articles/peak_calling).

The pipeline implements peak calling using both cellranger and signac. The latter one is preferred, especially the single cell (i.e. cell-type level pseudobulk) transferlabels approach.
The subsequent steps follow the same code as bulk, with the exception that Intervene has hard time dealing with many samples at the same time. This might be addressed in the future by using another implementation of Intervene, currenlty under development.

## Downstream functional analysis ##

We summarize resulting peaks and export them in a folder together with a summary table, describind those peaks. This is essential, since all the downstream analysis is done on these peaks. The peak summary step in addition to the peak called with different methods also geerates top100, top500 and top1000 peaks, which allows us to look into the strongest peaks and analyse them as well. In addition, we export the overlapping peaks between the different TFs used in the analysis or for the same TF, but in different conditions (samples). This also allows us to look into similarities between the TF and samples.

For downstream analysis we perform GREAT analysis, using rGREAT R package. Also we associate the called peak regions with gene regulatory regions using [GeneHancer](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5467550/) and [STITCHIT](https://github.com/SchulzLab/STITCHIT) annotations. Peaks are then annotated using ChIPseeker ([G Yu et al.](http://dx.doi.org/10.1093/bioinformatics/btv145)). Furthermore, we visualize the overlapping regions between the peak files using [Interve](https://intervene.readthedocs.io/en/latest/install.html).

## Output ##

The pipeline output multiple files. Different ones might be usefull to the user. In this section we will try to describe the main ones. **Note!** All alignment files (last ones being ```.bam``` files) are exported in the same folder where the original input data was found in subdirectory ```alignment```. The rest of the results are exported in a separate folder that is indicated in the ```config.yaml``` with ```analysis_title```.

### Alignment output ###

* ```fasta``` directory should contain the original input raw reads.
* ```alignment``` directory contains output from aligning process:
    1. Deduplicated ```.bam``` files.
    2. Fragment length information, which is used to summarize the alignment.
    3. Name-sorted ```.bam``` files.
    4. Picard summary files with duplicate information, which is later summarized in a plot.
    5. Sorted ```.bam``` files.
    6. ```.sam``` files.
    7. Bowtie2 alignment summaries, which are later used to make a summary plot.

```bash
data_directory
├── alignment
│   ├── bam
│   │   ├── deduped
│   │   ├── fragment_len
│   │   ├── nsorted
│   │   ├── picard_summary
│   │   └── sorted
│   └── sam
│       └── bowtie2_summary
└── fasta
```

### Results of peak calling and functional analysis of peaks ###

The structure of results directory can be found here. It includes the alignment summaries, data quality assesment summaries, peak calling results and their summaries. Finally, all the downstream functional analysis.

* BULK-specific output:

    1. the alignment summary from BULK reads alignment.
    2. BED and BEDGRAPH files prepared for peak calling.
    3. Read quality report (```fastq_report```).
    4. Peak calling with [MACS2](https://pypi.org/project/MACS2/) and [SEACR](https://github.com/FredHutch/SEACR) methods. MACS2 returns the peaks already in the ```.narrowpeak``` format, while SEACR peaks are converted into one by making two sets of peaks: **all_signal**, meaning the broader peaks where all signal of a called peak is distributed, and **max_signal**, meaning the regions where maximum of the signal is found, but essentially these two mean the same peak instances. Furthermore, peaks with SEACR are called with two thresholds - **relaxed** and **stringent**.

```bash
analysis_title
├── bulk
│   ├── alignment_summary
│   │   ├── alignment_summary_plots.pdf
│   │   └── correlation_between_replicates.pdf
│   ├── bed
│   │   ├── sample1.bed
│   │   ├── sample1.clean.bed
│   │   ├── sample1.fragments.bed
│   │   ├── sample1.fragments.bin.bed
│   │   └── ...
│   ├── bedgraph
│   │   ├── sample1.bedgraph
│   │   └── ...
│   ├── fastq_report
│   │   ├── sample1.nsorted_fastqc.html
│   │   └── ...
│   └── peak_calling
│       ├── MACS
│       └── SEACR
│           ├── relaxed
│           ├── relaxed_all_signal
│           ├── relaxed_max_signal
│           ├── stringent
│           ├── stringent_all_signal
│           └── stringent_max_signal
```

* peak calling summary:

    1. Tables with a list of peak files, including the top100, top500 and top1000 peaks for each calling method. Furthermore, overlapping peaks between different TFs and between the conditions for the same TF are found.
    2. Peak numbers, represented in the plot.
    3. A directory with all peak files described in the summary tables.

```bash
analysis_title
├── peak_calling_summary
│   ├── overlapping_peak_files_info_top1000.tsv
│   ├── overlapping_peak_files_info.tsv
│   ├── peak_files_info_top1000.tsv
│   ├── peak_files_info_top100.tsv
│   ├── peak_files_info_top500.tsv
│   ├── peak_files_info.tsv
│   ├── peak_summary_plot.pdf
│   └── peak_width_plot.pdf
└── sample_peaks
│   └── bulk
│       ├── MACS_from-bedpe_sample1.narrowPeak
│       └── ...
```

* downstream-analysis output:

    1. GREAT analysis output for each peak file.
    2. [Intervene](https://intervene.readthedocs.io/en/latest/install.html) output with plots representing different overlaps between the peaks.
    3. Peak assotiations with gene reulatory elements. These assotiations are made using [GeneHancer](https://www.ncbi.nlm.nih.gov/pmc/articles/PMC5467550/) and [STITCHIT](https://github.com/SchulzLab/STITCHIT) annotations.

```bash
analysis_title
├── GREAT_analysis
│   └── bulk
│       ├── MACS_from-bedpe_sample1
│       └── ...
├── intervene_plots
│   ├── DEAF1_upset_combinations.txt
│   ├── DEAF1_upset.pdf
│   └── DEAF1_upset.R
├── associations
│   └── bulk
│       ├── MACS_from-bedpe_AIRE-3095
│       │   ├── associations_genehancer_with-distance.bed
│       │   ├── associations_stitchit_with-distance.bed
│       │   ├── genehancer_full_list_of_associated_genes.txt
│       │   ├── genehancer_genes_of_interest_with_regions.txt
│       │   ├── genehancer_regions_for_genes_of_interest.bed
│       │   ├── stitchit_full_list_of_associated_genes.txt
│       │   ├── stitchit_genes_of_interest_with_regions.txt
│       │   └── stitchit_regions_for_genes_of_interest.bed
│       ├── SEACR_relaxed-all-signal_AIRE-3093_top100
│       │   └── ...
│       └── ...
└── peak_annotation
    ├── peak_annotation_chipseeker.pdf
    └── ...
```

## Quick start

Once you set the *config.yaml* file, run **Snakefile** script to launch the pipeline.

```unix
## Assuming you are in the pipeline_cutntag folder.

snakemake --cores 1
```

Check the [**Snakemake** documentation](https://snakemake.readthedocs.io/en/stable/index.html) for any question related to *Snakemake*.

##### The end of README.md
